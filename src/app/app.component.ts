import { Component } from '@angular/core'
import { SharedService } from './shared.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(public ss: SharedService) {}
}
