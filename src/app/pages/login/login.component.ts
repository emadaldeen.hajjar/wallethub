import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { SharedService } from '../../shared.service'
import { environment } from '../../../environments/environment'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  loading: boolean
  hide: boolean

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    public ss: SharedService
  ) {
    this.loginForm = fb.group({
      email: [
        localStorage.getItem('email'),
        [Validators.required, Validators.email]
      ],
      password: ['', [Validators.required, Validators.minLength(2)]]
    })

    this.hide = true
    this.loading = false
  }

  doLogin() {
    if (!this.loginForm.valid) return

    this.loading = true

    this.http
      .post(`${environment.apiUrl}login`, this.loginForm.value, {
        observe: 'response'
      })
      .subscribe(
        async res => {
          const email = this.loginForm.controls.email.value
          localStorage.setItem('email', email)
          this.ss.email = email

          await this.router.navigate([
            this.route.snapshot.queryParams.returnUrl || '/dashboard'
          ])
        },
        err => {
          this.loading = false
          if (err.status === 401) {
            // Show error message
          }
        }
      )
  }

  ngOnInit() {}
}
