import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-page-three',
  templateUrl: './page-three.component.html',
  styleUrls: ['./page-three.component.scss']
})
export class PageThreeComponent implements OnInit {
  public price: string

  constructor() {}

  receivePrice(price: string) {
    this.price = price
  }

  ngOnInit() {
    this.price = '1234'
  }
}
