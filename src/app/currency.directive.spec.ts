import { Component, DebugElement } from '@angular/core'
import {
  ComponentFixture,
  TestBed,
  ComponentFixtureAutoDetect
} from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { CurrencyDirective } from './currency.directive'

describe('CurrencyDirective', () => {
  let component: CurrencyComponent
  let fixture: ComponentFixture<CurrencyComponent>
  let inputEl: DebugElement

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CurrencyDirective, CurrencyComponent]
    }).compileComponents()

    fixture = TestBed.createComponent(CurrencyComponent)
    component = fixture.componentInstance
    inputEl = fixture.debugElement.query(By.directive(CurrencyDirective))
    fixture.detectChanges()
  })

  it('should create an instance', async () => {
    console.log('directive', inputEl.nativeElement.value)
    await expect(inputEl).toBeTruthy()
  })

  it('valued shoud be formated', async () => {
    await expect(inputEl.nativeElement.value).toBe('#1,234')
  })
})

@Component({
  selector: 'app-currency',
  template: `
    <input appCurrency value="1234" prefix="#" />
  `
})
export class CurrencyComponent {}
