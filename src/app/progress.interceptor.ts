import { Injectable } from '@angular/core'
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpEvent,
  HttpEventType
} from '@angular/common/http'
import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'
import { SharedService } from './shared.service'

@Injectable()
export class ProgressInterceptor implements HttpInterceptor {
  constructor(public ss: SharedService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const clone = req.clone({
      reportProgress: true
    })

    return next.handle(clone).pipe(
      tap(
        evt => {
          this.ss.error = false
          if (clone.method === 'GET') {
            if (evt.type === HttpEventType.Sent) {
              // this.ss.percentDone = 0;
            } else if (evt.type === HttpEventType.DownloadProgress) {
              if (evt.total) {
                this.ss.percentDone = Math.round((100 * evt.loaded) / evt.total)
              }
            }
          } else {
            if (evt.type === HttpEventType.UploadProgress) {
              if (evt.total) {
                this.ss.percentDone = Math.round(
                  (100 * evt.loaded) / evt.total / 2
                )
              }
            } else if (evt.type === HttpEventType.DownloadProgress) {
              if (evt.total) {
                this.ss.percentDone =
                  Math.round((100 * evt.loaded) / evt.total / 2) + 50
              }
            }
          }

          if (evt instanceof HttpResponse) {
            this.ss.percentDone = 100
            if (evt.status !== 200) {
            }
          }
        },
        () => {
          this.ss.percentDone = 100
          // this.ss.error = true;
        }
      )
    )
  }
}
