import { Component, OnInit, OnChanges, Input } from '@angular/core'

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit, OnChanges {
  @Input() starCount = 5
  @Input() rate = 0

  public ratingArr: string[]

  constructor() {}

  buildRating() {
    this.ratingArr = []
    for (let i = 0; i < this.starCount; i += 1) {
      this.ratingArr.push(i >= this.rate ? 'star_border' : 'star')
    }
  }

  ngOnInit() {
    this.buildRating()
  }

  ngOnChanges() {
    this.buildRating()
  }
}
